package org.uom.fit.level2.datavis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataVisApplication {

    private static final Logger log = LoggerFactory.getLogger(DataVisApplication.class);

    public static void main(String[] args) {
        log.info("starting the datavis application");
        SpringApplication.run(DataVisApplication.class, args);
    }
}
